<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Cultureel Studentencentrum USVA</title>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">

    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="css/jquery.smartmenus.bootstrap.css" rel="stylesheet">

    <!-- USVA Theme build upon Bootstrap -->
    <link href="css/style.css" rel="stylesheet">



    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Oswald:700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

  </head>
  <body>


  <section id="nav-top">

    <div class="container">

      <div class="pull-right">

        <div class="pull-right">


          <a href="#" class="ml10 btn btn-default mr15">En<span class="hidden-xs">glish</span></a>

          <a href="zoeken.php" class="btn btn-default mr15 hidden-sm hidden-md hidden-lg hidden-xl"><i class="fa fa-search"></i></a>

          <a href="#" class="btn btn-default mr15">Login</a>

          <a href="#" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a>

          <!-- <a class="btn btn-link"><i class="fa fa-lg fa-shopping-cart"></i></a> -->
        </div>

        <form action="zoeken.php" id="search-form" method="get" class="pull-left form-inline mr10 hidden-xs">
          <div class="input-group">
            <input type="search" name="s" class="form-control searchbar-grow pull-right" placeholder="">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button" value="submit"><i class="fa fa-search"></i>&nbsp;</button>
            </span>
          </div><!-- /input-group -->
        </form>

      </div>

      <div class="pull-right social hidden-xs">

        <a href="https://www.facebook.com/Cultureelstudentencentrumusva/" target="_blank"><i class="fa fa-lg fa-facebook-official mr15"></i></a>
        <a href="https://twitter.com/usva_cultuur" target="_blank"><i class="fa fa-lg fa-twitter mr15"></i></a>
        <a href="https://www.instagram.com/usva_cultuur/" target="_blank"><i class="fa fa-lg fa-instagram mr15"></i></a>
        <a href="https://nl.linkedin.com/in/usva-cultureel-studentencentrum-9a3367109" target="_blank"><i class="fa fa-linkedin mr15"></i></a>

      </div>

    </div>



  </section>





  <section id="nav-main" class="relative">

    <div class="container">

      <div class="row">

        <nav class="navbar navbar-default m0 p0 b0" role="navigation">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">

              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
              </button>

              <a id="nav-main-logo" class="navbar-brand" href="/"><img src="img/logo-usva-out-sm.png" /></a>
            </div>

            <!-- Collect the nav links and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar">

              <ul class="nav navbar-nav">

                <li class="active"><a href="agenda.php">Agenda</a></li>
                <li><a href="cursussen.php">Cursussen</a></li>





                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cultuurkoepel <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="pagina.php">Pagina</a></li>
                    <li><a href="pagina.php">Een link naar een pagina</a></li>
                    <li><a href="pagina.php">Een andere link</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="pagina.php">Subsectie link</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="dropdown-submenu">

                      <a href="#">Opties onder een submenu <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                          <li><a href="#">Eerste pagina</a></li>
                          <li><a href="#">Tweede langere pagina onder sub</a></li>
                          <li><a href="#">Nog eentje om het af te leren</a></li>
                      </ul>
                    </li>

                  </ul>
                </li>
                <li><a href="pagina.php">Zakelijk</a></li>
                <li><a href="pagina.php">Over USVA</a></li>
                <li><a href="contact.php">Contact</a></li>

              </ul>

            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

      </div>   

    </div>

  </section>



  <header class="relative" style="background: url(img/demo.jpg) no-repeat bottom center;">

    <div class="shadow-overlay"></div>

    <div class="container header-logo hidden-xs">
      <a href="/"><img src="img/logo-usva.png" /></a>
    </div> 3

    <div class="container header-text">
      <h1 class="pull-right text-white text-shadow"><a href="">Agenda</a></h1>
    </div>

  </header>


  <div id="particles-js"></div>
