    <?php include('header.php'); ?>
    


      <div id="particles-js"></div>


      <div class="container">

        <section class="row">
          
          <div class="col-md-8">

            <article class="usva-panel">

              <h2 class="title">Laatste nieuws</h2>

              <div class="meta">
                <p class="text-caps-light">Geplaats op 16 juli 2015</p>
              </div>

              <div class="content">
                <p>
                Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. Deze molen wekt genoeg energie op voor zo'n acht huishoudens. Hun bedrijfje, EAZ Wind in het Groningse Overschild, verovert met de uitvinding momenteel het Groningse platteland. Tientallen molens hebben ze al verkocht.
                </p>
                <p>
                Volgend jaar hopen ze er dertig te produceren. Maar hun droom reikt natuurlijk verder dan Groningen: ook elders in Nederland zouden de houten molens moeten verschijnen.
                </p>
                <p>            
                "We krijgen veel aanvragen, ook bijvoorbeeld uit Friesland. Maar de bestemmingsplannen daar staan kleine windmolens van 15 meter (nog) niet toe", zegt Timo Spijkerboer van EAZ ("staat voor Enschede aan Zee, is een grapje uit onze studententijd, omdat we liever aan zee woonden met veel wind").
                </p>
                <h3>Het groepsproces</h3>              
                <p>
                Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. Deze molen wekt genoeg energie op voor zo'n acht huishoudens. Hun bedrijfje, EAZ Wind in het Groningse Overschild, verovert met de uitvinding momenteel het Groningse platteland. Tientallen molens hebben ze al verkocht.
                </p>
                <p>
                Volgend jaar hopen ze er dertig te produceren. Maar hun droom reikt natuurlijk verder dan Groningen: ook elders in Nederland zouden de houten molens moeten verschijnen.
                </p>
                <p>            
                "We krijgen veel aanvragen, ook bijvoorbeeld uit Friesland. Maar de bestemmingsplannen daar staan kleine windmolens van 15 meter (nog) niet toe", zegt Timo Spijkerboer van EAZ ("staat voor Enschede aan Zee, is een grapje uit onze studententijd, omdat we liever aan zee woonden met veel wind").
                </p>
                <h3>Samen staan we sterker</h3>
                <p>
                Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. Deze molen wekt genoeg energie op voor zo'n acht huishoudens. Hun bedrijfje, EAZ Wind in het Groningse Overschild, verovert met de uitvinding momenteel het Groningse platteland. Tientallen molens hebben ze al verkocht.
                </p>
                <p>
                Volgend jaar hopen ze er dertig te produceren. Maar hun droom reikt natuurlijk verder dan Groningen: ook elders in Nederland zouden de houten molens moeten verschijnen.
                </p>
                <p>            
                "We krijgen veel aanvragen, ook bijvoorbeeld uit Friesland. Maar de bestemmingsplannen daar staan kleine windmolens van 15 meter (nog) niet toe", zegt Timo Spijkerboer van EAZ ("staat voor Enschede aan Zee, is een grapje uit onze studententijd, omdat we liever aan zee woonden met veel wind").
                </p>            
              </div>
            </article>

          </div>

          <div class="col-md-4">
            <div class="usva-widget widget-agenda">
              <h2 class="title">Agenda</h2>

              <table class="table">
                <tr>
                  <td class="date"><span class="day">14</span><span class="month">Nov</span></td>
                  <td class="item">Open Jazz Session</td>
                </tr>
                <tr>
                  <td class="date"><span class="day">15</span><span class="month">Dec</span></td>
                  <td class="item">Mira in Concert</td>
                </tr>
                <tr>
                  <td class="date"><span class="day">1</span><span class="month">Jan</span></td>
                  <td class="item">Eindoptreden Estrellas</td>
                </tr>                                
              </table>

              <a class="more" href="">Bekijk de hele agenda<i class="fa fa-arrow-right ml10"></i></a>

            </div>
          </div>

          <div class="col-md-4">
            <div class="usva-widget widget-red">
              <h2 class="title">USVA Nieuwsbrief</h2>
            </div>
          </div>

          <div class="col-md-4">
            <div class="usva-widget widget-red">
              <h2 class="title">Schrijf je in voor een cursus</h2>
            </div>
          </div>

          <div class="col-md-4">
            <div class="usva-widget widget-red">
              <h2 class="title">Vraag subsidie aan</h2>
            </div>
          </div>

        </section>

      </div>









    <?php include('footer.php'); ?>
