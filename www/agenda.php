    <?php include('header.php'); ?>
    


      <div id="particles-js"></div>


      <div class="container">

        <section class="row">


          <div class="col-md-8 col-md-push-4">

            <article class="usva-panel panel-agenda">

              <h2 class="title">November <span class="year">2015</span></h2>

              <div class="table-responsive">
                <table class="table">

                  <tr data-href="agenda-item.php">
                    <td class="date"><span class="dayotw">Ma</span><span class="day">14 Nov</span></td>
                    <td class="img hidden-xs"><img src="http://lorempixel.com/135/135/nightlife/5" /></td>
                    <td class="item">
                      <h2>Open Jazz Session</h2>
                      <span class="time mr20"><i class="fa fa-lg fa-clock-o mr10"></i>19:30</span>
                      <span class="location"><i class="fa fa-lg fa-map-marker mr10"></i>USVA Cafe</span>
                      <span class="tag"><i class="fa fa-lg fa-tag mr10"></i>Muziek</span>
                      <span class="go">Gratis</span>
                    </td>
                  </tr>
                  <tr data-href="agenda-item.php">
                    <td class="date"><span class="dayotw">Di</span><span class="day">15 Dec</span></td>
                    <td class="img hidden-xs"><img src="http://lorempixel.com/135/135/nightlife/3" /></td>                  
                    <td class="item">
                      <h2>Mira in Concert</h2>
                      <span class="time mr20"><i class="fa fa-lg fa-clock-o mr10"></i>20:15</span>
                      <span class="location"><i class="fa fa-lg fa-map-marker mr10"></i>Huis de Beurs</span>
                      <span class="tag"><i class="fa fa-lg fa-tag mr10"></i>Muziek</span>
                      <span class="go">&euro; 17,50<i class="fa fa-arrow-right ml10"></i></span>
                    </td>
                  </tr>
                  <tr data-href="agenda-item.php">
                    <td class="date"><span class="dayotw">Vr</span><span class="day">1 Jan</span></td>
                    <td class="img hidden-xs"><img src="http://lorempixel.com/135/135/nightlife/1" /></td>                  
                    <td class="item">
                      <h2>Nieuwjaarsoptreden 2016 Estrellas + support</h2>
                      <span class="time mr20"><i class="fa fa-lg fa-clock-o mr10"></i>19:30</span>
                      <span class="location"><i class="fa fa-lg fa-map-marker mr10"></i>USVA Grote Zaal</span>
                      <span class="tag"><i class="fa fa-lg fa-tag mr10"></i>Muziek</span>
                      <span class="go">Afgelast</span>                   
                    </td>
                  </tr>   
                  <tr data-href="agenda-item.php">
                    <td class="date"><span class="dayotw">Ma</span><span class="day">14 Nov</span></td>
                    <td class="img hidden-xs"><img src="http://lorempixel.com/135/135/nightlife/5" /></td>
                    <td class="item">
                      <h2>Theatraal liedjesprogramma: maak het donker in het donker</h2>
                      <span class="time mr20"><i class="fa fa-lg fa-clock-o mr10"></i>20:30</span>
                      <span class="location"><i class="fa fa-lg fa-map-marker mr10"></i>Theater de Gulle Lach</span>
                      <span class="tag"><i class="fa fa-lg fa-tag mr10"></i>Theater</span>
                      <span class="go">Uitverkocht</span>    
                    </td>
                  </tr>
                  <tr data-href="agenda-item.php">
                    <td class="date"><span class="dayotw">Ma</span><span class="day">14 Nov</span></td>
                    <td class="img hidden-xs"><img src="http://lorempixel.com/135/135/nightlife/5" /></td>
                    <td class="item">
                      <h2>Open Jazz Session</h2>
                      <span class="time mr20"><i class="fa fa-lg fa-clock-o mr10"></i>19:30</span>
                      <span class="location"><i class="fa fa-lg fa-map-marker mr10"></i>USVA Cafe</span>
                      <span class="tag"><i class="fa fa-lg fa-tag mr10"></i>Muziek</span>
                      <span class="go">v.a. &euro; 7,50<i class="fa fa-arrow-right ml10"></i></span>                      
                    </td>
                  </tr>
                  <tr data-href="agenda-item.php">
                    <td class="date"><span class="dayotw">Di</span><span class="day">15 Dec</span></td>
                    <td class="img hidden-xs"><img src="http://lorempixel.com/135/135/nightlife/3" /></td>                  
                    <td class="item">
                      <h2>Mira in Concert</h2>
                      <span class="time mr20"><i class="fa fa-lg fa-clock-o mr10"></i>20:15</span>
                      <span class="location"><i class="fa fa-lg fa-map-marker mr10"></i>Huis de Beurs</span>
                      <span class="tag"><i class="fa fa-lg fa-tag mr10"></i>Muziek</span>
                      <span class="go">v.a. &euro; 5,-<i class="fa fa-arrow-right ml10"></i></span>                                            
                    </td>
                  </tr>
                  <tr data-href="agenda-item.php">
                    <td class="date"><span class="dayotw">Vr</span><span class="day">1 Jan</span></td>
                    <td class="img hidden-xs"><img src="http://lorempixel.com/135/135/nightlife/1" /></td>                  
                    <td class="item">
                      <h2>Nieuwjaarsoptreden 2016 Estrellas + support</h2>
                      <span class="time mr20"><i class="fa fa-lg fa-clock-o mr10"></i>19:30</span>
                      <span class="location"><i class="fa fa-lg fa-map-marker mr10"></i>USVA Grote Zaal</span>
                      <span class="tag"><i class="fa fa-lg fa-tag mr10"></i>Muziek</span>
                      <span class="go">Gratis</span>                                                             
                    </td>
                  </tr>   
                  <tr data-href="agenda-item.php">
                    <td class="date"><span class="dayotw">Ma</span><span class="day">14 Nov</span></td>
                    <td class="img hidden-xs"><img src="http://lorempixel.com/135/135/nightlife/5" /></td>
                    <td class="item">
                      <h2>Theatraal liedjesprogramma: maak het donker in het donker</h2>
                      <span class="time mr20"><i class="fa fa-lg fa-clock-o mr10"></i>20:30</span>
                      <span class="location"><i class="fa fa-lg fa-map-marker mr10"></i>Theater de Gulle Lach</span>
                      <span class="tag"><i class="fa fa-lg fa-tag mr10"></i>Theater</span>
                      <span class="go">Gratis</span>                                                                                      
                    </td>
                  </tr>  

                </table>
              </div>

            </article>

          </div>



          <div class="col-md-4 col-md-pull-8">

            <div class="usva-widget widget-datepicker table-responsive">

              <h2 class="title">Overzicht</h2>

              <div id="datepicker"></div>

            </div>

          </div>



        </section>

      </div>









    <?php include('footer.php'); ?>
