$(document).ready(function(){


  $('html, body').on('mousewheel' , function(){}) // resolves the flickering issue with sticky.js

  
  $("#nav-main").sticky({topSpacing:0}); // makes the nav-main stick
  

  // makes logo appear after scrolldown
  $(window).scroll(function() {
    if ($(this).scrollTop() > 275){  
      $('#nav-main-logo').addClass("display");
    }
    else{
      $('#nav-main-logo').removeClass("display");
    }
  }); 


  // makes links with data-href attribute clickable
  $('tr[data-href]').on("click", function() {
      document.location = $(this).data('href');
  });


  // datepicker for agenda
  var active_dates = ["16/11/2015", "21/11/2015","29/11/2015"]; // we should take this from the agenda database or external json

  $('#datepicker').datepicker({
    language: "nl",
    keyboardNavigation: false,
    format: "dd/mm/yyyy",
    autoclose: true,
    todayHighlight: true,
    beforeShowDay: function(date){
      var d = date;
      var curr_date = d.getDate();
      var curr_month = d.getMonth() + 1; // months are zero based
      var curr_year = d.getFullYear();
      var formattedDate = curr_date + "/" + curr_month + "/" + curr_year

      if ($.inArray(formattedDate, active_dates) != -1){
        return {
          classes: 'active-day'
        };
      }
      return;
    }
  });


  /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
  particlesJS.load('particles-js', 'assets/particles.json', function() {
    console.log('callback - particles.js config loaded');
  });


});