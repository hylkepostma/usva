    <?php include('header.php'); ?>
    


      <div id="particles-js"></div>


      <div class="container">

        <section class="row">
          
          <div class="col-md-12">

            <article class="usva-panel usva-panel-col-12">

              <h2 class="title">Tekenen en schilderen <span>beginners</span></h2>


              <section class="row">
                
                <div class="col-md-8">

                  <!--                   
                  <div class="meta">
                    <p class="text-caps-light">Geplaatst op 16 juli 2015</p>
                  </div> 
                  -->

                  <div class="content">
                    <p>
                    Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. Deze molen wekt genoeg energie op voor zo'n acht huishoudens. Hun bedrijfje, EAZ Wind in het Groningse Overschild, verovert met de uitvinding momenteel het Groningse platteland. Tientallen molens hebben ze al verkocht.
                    </p>    
                    <p>
                    Volgend <b>jaar</b> hopen ze er <strong>dertig</strong> te <i>produceren</i>. Maar hun <u>droom</u> reikt natuurlijk verder dan Groningen: ook elders in Nederland zouden de houten molens moeten verschijnen.
                    </p>
                    <h3>Wat doe je?</h3>              
                    <p>            
                    "We krijgen veel aanvragen, ook bijvoorbeeld uit Friesland. Maar de bestemmingsplannen daar staan kleine windmolens van 15 meter (nog) niet toe", zegt Timo Spijkerboer van EAZ ("staat voor Enschede aan Zee, is een grapje uit onze studententijd, omdat we liever aan zee woonden met veel wind").
                    </p>
                    <h3>Ingangseisen</h3>              
                    <p>
                    Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. Deze molen wekt genoeg energie op voor zo'n acht huishoudens. Hun bedrijfje, EAZ Wind in het Groningse Overschild, verovert met de uitvinding momenteel het Groningse platteland. Tientallen molens hebben ze al verkocht.
                    </p>
                    <h3>Expositie</h3> 
                    <p>
                    Volgend jaar hopen ze er dertig te produceren. Maar hun droom reikt natuurlijk verder dan Groningen: ook elders in Nederland zouden de houten molens moeten verschijnen.
                    </p>
                    <p>            
                    "We krijgen veel aanvragen, ook bijvoorbeeld uit Friesland. Maar de bestemmingsplannen daar staan kleine windmolens van 15 meter (nog) niet toe", zegt Timo Spijkerboer van EAZ ("staat voor Enschede aan Zee, is een grapje uit onze studententijd, omdat we liever aan zee woonden met veel wind").
                    </p>
                    <h3>Bijzonderheden</h3> 
                    <p>
                    Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. Deze molen wekt genoeg energie op voor zo'n acht huishoudens. Hun bedrijfje, EAZ Wind in het Groningse Overschild, verovert met de uitvinding momenteel het Groningse platteland. Tientallen molens hebben ze al verkocht.
                    </p>
                    <p>
                    Volgend jaar hopen ze er dertig te produceren. Maar hun droom reikt natuurlijk verder dan Groningen: ook elders in Nederland zouden de houten molens moeten verschijnen.
                    </p>
                    <p>            
                    "We krijgen veel aanvragen, ook bijvoorbeeld uit Friesland. Maar de bestemmingsplannen daar staan kleine windmolens van 15 meter (nog) niet toe", zegt Timo Spijkerboer van EAZ ("staat voor Enschede aan Zee, is een grapje uit onze studententijd, omdat we liever aan zee woonden met veel wind").
                    </p>            
                  </div>

                </div>

                <div class="col-md-4">
                

                  <div class="widget-course table-responsive">

                    <table class="table">
                      
                      <thead>
                        <tr>
                          <th colspan="2"><h4>eerste trimester</h4></th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td>Start</td>
                          <td>15 december 2015</td>
                        </tr>
                        <tr>
                          <td>Duur</td>
                          <td>10 weken</td>
                        </tr>
                        <tr>
                          <td>Tijd</td>
                          <td>Ma 18:30-20:30</td>
                        </tr>
                        <tr>                          
                          <td>Prijs</td>
                          <td>&euro;115 / &euro;146 / &euro;170</td> 
                        </tr>                           
                        <tr>
                          <td>Docent</td> 
                          <td>Ellis Veldstra </td> 
                        </tr>
                        <tr>                          
                          <td>Deelnemers</td> 
                          <td>16</td> 
                        </tr>                                                                    
                      </tbody>

                      <tfoot>
                        <tr>
                          <td colspan="2" class="p0"><a href="" class="btn btn-red btn-block btn-xl">Schrijf je in</a></td>
                        </tr>
                      </tfoot>

                    </table>

                  </div>


                  <div class="widget-course table-responsive">

                    <table class="table">
                      
                      <thead>
                        <tr>
                          <th colspan="2"><h4 class="text-center">tweede trimester</h4></th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td>Start</td>
                          <td>15 december 2015</td>
                        </tr>
                        <tr>
                          <td>Duur</td>
                          <td>10 weken</td>
                        </tr>
                        <tr>
                          <td>Tijd</td>
                          <td>Ma 18:30-20:30</td>
                        </tr>
                        <tr>                          
                          <td>Prijs</td>
                          <td>&euro;115 / &euro;146 / &euro;170</td> 
                        </tr>                           
                        <tr>
                          <td>Docent</td> 
                          <td>Ellis Veldstra </td> 
                        </tr>
                        <tr>                          
                          <td>Deelnemers</td> 
                          <td>16</td> 
                        </tr>                                                                    
                      </tbody>

                      <tfoot>
                        <tr>
                          <td colspan="2" class="p0"><a href="" class="btn btn-red btn-block btn-xl">Schrijf je in</a></td>
                        </tr>
                      </tfoot>

                    </table>

                  </div>


                </div>

              </section>

            </article>

          </div>



        </section>

      </div>









    <?php include('footer.php'); ?>
