      <footer>

        <div class="container">

          <div class="col-md-2 col-sm-3">
            <a href="/"><img class="img-responsive logo" src="img/logo-usva.png" /></a>
          </div>

          <div class="col-md-2 ml-minus hidden-xs hidden-sm">
            <ul class="li0">
              <li class="text-caps">Handige links</li>
              <li><a href="">Agenda</a></li>
              <li><a href="">Bewoners</a></li>          
              <li><a href="">Café</a></li>
              <li><a href="">Commissies</a></li>
              <li><a href="">Cursussen</a></li>           
              <li><a href="">Kaartverkoop</a></li>
              <li><a href="">Nieuwsbrief</a></li>              
            </ul>
          </div>

          <div class="col-md-2 mr-plus hidden-xs hidden-sm">
            <ul class="li0">
              <li class="text-caps">&nbsp;</li>
              <li><a href="">Organisatie</a></li>
              <li><a href="">Partners</a></li>
              <li><a href="">Routebeschrijving</a></li>
              <li><a href="">USVA Magazine</a></li>
              <li><a href="">Vacatures</a></li>                            
              <li><a href="">Verhuur</a></li>
            </ul>          
          </div>

          <div class="col-md-2  col-sm-4">
            <ul class="li0">
              <li class="text-caps">Openingstijden</li>
              <li>Ma-vrij: 9:00 - 23:00</li>
              <li class="text-caps">&nbsp;</li>
              <li class="text-caps">Volg ons</li>
              <li><a href="https://www.facebook.com/Cultureelstudentencentrumusva/" target="_blank"><i class="fa fa-facebook-official mr10 mb10"></i>Facebook</a></li>
              <li><a href="https://twitter.com/usva_cultuur" target="_blank"><i class="fa fa-twitter mr10 mb10"></i>Twitter</a></li>
              <li><a href="https://www.instagram.com/usva_cultuur/" target="_blank"><i class="fa fa-instagram mr10 mb10"></i>Instagram</a></li>
              <li><a href="https://nl.linkedin.com/in/usva-cultureel-studentencentrum-9a3367109" target="_blank"><i class="fa fa-linkedin mr10 mb10"></i>LinkedIn</a></li>
            </ul>
          </div>

          <div class="col-md-4 col-sm-5 text-right ml-minus">
            <ul class="li0">
              <li class="text-caps">Contactgegevens</li>
              Cultureel studentencentrum Usva<br>
              Munnekeholm 10<br>
              9711 JA Groningen<br><br>
              usva@rug.nl<br>
              050 363 4670</li>
            </ul>
          </div>
        </div>

      </footer>


      <div class="colofon">

        <div class="container text-center">
          Alle rechten voorbehouden · 2015
        </div>

      </div>



      <!-- jQuery -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="js/bootstrap.min.js"></script>
      <!-- <script src="js/jquery.bootstrap-dropdown-hover.min.js"></script> -->
      <script src="js/jquery.smartmenus.min.js"></script>
      <script src="js/jquery.smartmenus.bootstrap.min.js"></script>
      <script src="js/bootstrap-datepicker.min.js"></script>
      <script src="js/bootstrap-datepicker.nl.min.js"></script>
      <script src="js/jquery.sticky.js"></script>
      <script src="js/particles.min.js"></script>
      <script src="js/salvattore.min.js"></script>


      <script src="js/scripts.js"></script>


    </body>
  </html>