    <?php include('header.php'); ?>
    


      <div class="container">

        <section class="row">
          
          <div class="col-md-8">

            <article class="usva-panel panel-news-snippet">

              <h2 class="title">Laatste nieuws</h2>

              <div class="meta">
                <p class="text-caps-light">Geplaatst op 16 juli 2015</p>
              </div>

              <div class="content">
                <p>
                Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. Deze molen wekt genoeg energie op voor zo'n acht huishoudens. Hun bedrijfje, EAZ Wind in het Groningse Overschild, verovert met de uitvinding momenteel...
                </p> 
              </div>

              <a href="nieuws-item.php" class="go">Lees verder<i class="fa fa-arrow-right ml10"></i></a>    

            </article>

            <article class="usva-panel panel-news-snippet">

              <img class="img-responsive" src="http://lorempixel.com/800/450/nightlife/7" />

              <h2 class="title title-after-img">Nog meer laatste nieuws</h2>

              <div class="meta">
                <p class="text-caps-light">Geplaatst op 16 juli 2015</p>
              </div>

              <div class="content">
                <p>
                Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. 
                </p>
              </div>

              <a href="nieuws-item.php" class="go">Lees verder<i class="fa fa-arrow-right ml10"></i></a>    

            </article>


            <article class="usva-panel panel-news-snippet">

              <h2 class="title">Nog meer laatste nieuws</h2>

              <div class="meta">
                <p class="text-caps-light">Geplaatst op 16 juli 2015</p>
              </div>

              <div class="content">
                <p>
                Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. 
                </p> 
              </div>

              <a href="nieuws-item.php" class="go">Lees verder<i class="fa fa-arrow-right ml10"></i></a>    

            </article>


            <article class="usva-panel panel-news-snippet">

              <h2 class="title">Dat was dus eigenlijk niet het laatste nieuws, dit is het 'laatste' nieuws</h2>

              <div class="meta">
                <p class="text-caps-light">Geplaatst op 16 juli 2015</p>
              </div>

              <div class="content">
                <p>
                Deze molen wekt genoeg energie op voor zo'n acht huishoudens. Hun bedrijfje, EAZ Wind in het Groningse Overschild, verovert met de uitvinding momenteel het Groningse platteland. Tientallen molens hebben ze al verkocht.
                </p> 
              </div>

              <a href="nieuws-item.php" class="go">Lees verder<i class="fa fa-arrow-right ml10"></i></a>    

            </article>




          </div>

          <div class="col-md-4">
            <div class="usva-widget widget-agenda">
              <h2 class="title">Agenda</h2>

              <table class="table">
                
                <tr data-href="agenda-item.php">
                  <td class="date"><span class="day">14</span><span class="month">Nov</span></td>
                  <td class="item">Open Jazz Session</td>
                </tr>
                
                <tr data-href="agenda-item.php">
                  <td class="date"><span class="day">15</span><span class="month">Dec</span></td>
                  <td class="item">Mira in Concert</td>
                </tr>

                <tr data-href="agenda-item.php">
                  <td class="date"><span class="day">1</span><span class="month">Jan</span></td>
                  <td class="item">Eindoptreden Estrellas</td>
                </tr> 

              </table>

              <a class="more" href="agenda.php">Bekijk de hele agenda<i class="fa fa-arrow-right ml10"></i></a>

            </div>
          </div>

          <div class="col-md-4">
            <a href="#" class="usva-widget widget-red">
              <h2 class="title">USVA Nieuwsbrief</h2>
            </a>
          </div>

          <div class="col-md-4">
            <a href="#" class="usva-widget widget-red">
              <h2 class="title">Schrijf je in voor een cursus</h2>
            </a>
          </div>

          <div class="col-md-4">
            <a href="#" class="usva-widget widget-red">
              <h2 class="title">Vraag subsidie aan</h2>
            </a>
          </div>

        </section>

      </div>









    <?php include('footer.php'); ?>
