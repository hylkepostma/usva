    <?php include('header.php'); ?>
    


      <div id="particles-js"></div>


      <div class="container">

        <section class="row">
          
          <div class="col-md-12">

            <article class="usva-panel usva-panel-col-12">

              <h2 class="title">Cursussen</h2>


              <section class="row">
                
                <div class="col-md-8">

                  <div class="content">

                    <div class="row mb10">
                      
                      <div class="col-md-12">                  
                        <p>
                        Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. Deze molen wekt genoeg energie op voor zo'n acht huishoudens.
                        </p>
                      </div>
                    </div>                 
                  
                  </div>

                  <div class="content">


                    <!-- Nav tabs -->
                    <ul class="nav nav-pills mt30" role="tablist">
                      <li class="hd hidden-xs">Sortering:</li>                    
                      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Alfabetisch</a></li>
                      <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Categorieën</a></li>
                    </ul>



                    <!-- Tab panes -->
                    <div class="tab-content mt50">
                      <div role="tabpanel" class="tab-pane active" id="home">

                        <div id="grid" data-columns>
                          
                          <div class="column">
                            <dl>
                              <dt>A</dt>
                              <dd><a href="/cursussen-item.php">Acteren (beginner)</a></dd>
                              <dd><a href="/cursussen-item.php">Acteren (vervolg)</a></dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>B</dt>
                              <dd>Basiscursus fotografie</dd>
                              <dd>Boetseren</dd>
                              <dd>Buikdansen</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>D</dt>
                              <dd>DJ cursus</dd>
                              <dd>DJ kaart</dd>
                              <dd>Drawing and painting</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>E</dt>
                              <dd>Egyptisch buikdansen</dd>
                              <dd>Egyptisch buikdansen (vervolg)</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>F</dt>
                              <dd>Film maken</dd>
                            </dl>
                          </div>
                          <div class="column">
                            <dl>
                              <dt>G</dt>
                              <dd>Acteren (beginner)</dd>
                              <dd>Acteren (vervolg)</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>H</dt>
                              <dd>Basiscursus fotografie</dd>
                              <dd>Boetseren</dd>
                              <dd>Buikdansen</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>I</dt>
                              <dd>DJ cursus</dd>
                              <dd>DJ kaart</dd>
                              <dd>Drawing and painting</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>K</dt>
                              <dd>Egyptisch buikdansen</dd>
                              <dd>Egyptisch buikdansen (vervolg)</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>M</dt>
                              <dd>Film maken</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>O</dt>
                              <dd>Acteren (beginner)</dd>
                              <dd>Acteren (vervolg)</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>P</dt>
                              <dd>Basiscursus fotografie</dd>
                              <dd>Boetseren</dd>
                              <dd>Buikdansen</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>R</dt>
                              <dd>DJ cursus</dd>
                              <dd>DJ kaart</dd>
                              <dd>Drawing and painting</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>S</dt>
                              <dd>Egyptisch buikdansen</dd>
                              <dd>Egyptisch buikdansen (vervolg)</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>W</dt>
                              <dd>Film maken</dd>
                            </dl>
                          </div>

                        </div> <!-- einde grid -->

                      </div> <!-- einde tabpanel -->


                      <div role="tabpanel" class="tab-pane" id="profile">
                        
                        <div id="grid" data-columns>
                          
                          <div class="column">
                            <dl>
                              <dt>Beeldend</dt>
                              <dd><a href="/cursussen-item.php">Acteren (beginner)</a></dd>
                              <dd><a href="/cursussen-item.php">Acteren (vervolg)</a></dd>
                              <dd>Basiscursus fotografie</dd>
                              <dd>Boetseren</dd>
                              <dd>Buikdansen</dd>                              
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>Body and Mind</dt>
                              <dd>Basiscursus fotografie</dd>
                              <dd>Boetseren</dd>
                              <dd>Buikdansen</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>Dans</dt>
                              <dd>DJ cursus</dd>
                              <dd>DJ kaart</dd>
                              <dd>Drawing and painting</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>Film en fotografie</dt>
                              <dd>Egyptisch buikdansen</dd>
                              <dd>Egyptisch buikdansen (vervolg)</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>Muziek</dt>
                              <dd>Film maken</dd>
                            </dl>
                          </div>
                          <div class="column">
                            <dl>
                              <dt>Pianolessen</dt>
                              <dd>Acteren (beginner)</dd>
                              <dd>Acteren (vervolg)</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>Theater</dt>
                              <dd>Basiscursus fotografie</dd>
                              <dd>Boetseren</dd>
                              <dd>Buikdansen</dd>
                            </dl>
                          </div>

                          <div class="column">
                            <dl>
                              <dt>Woord</dt>
                              <dd>DJ cursus</dd>
                              <dd>DJ kaart</dd>
                              <dd>Drawing and painting</dd>
                            </dl>
                          </div>

                        </div> <!-- einde grid -->


                      </div> <!-- einde tabpanel -->



                    </div> <!-- einde tab-content -->


                  </div> <!-- einde content -->


                </div> <!-- einde col-md-8 -->

                <div class="col-md-4">
                
                  <img class="img-responsive img-border" src="http://lorempixel.com/800/800/nightlife/9">

                </div>

              </section>

            </article>

          </div>



        </section>

      </div>









    <?php include('footer.php'); ?>
