    <?php include('header.php'); ?>
    


      <div id="particles-js"></div>


      <div class="container">

        <section class="row">







          <div class="col-md-8 col-md-push-4">

            <article class="usva-panel panel-agenda-item">



              <div class="img-header">

                <div class="item">
                  <h2 class="m0">Open Jazz Session</h2>
                  <span class="time mr20"><i class="fa fa-lg fa-clock-o mr10"></i>19:30</span>
                  <span class="location mr20"><i class="fa fa-lg fa-map-marker mr10"></i>USVA Cafe</span>
                  <span class="tag"><i class="fa fa-lg fa-tag mr10"></i>Muziek</span>
                </div>

                <img class="img-responsive" src="http://lorempixel.com/800/450/nightlife/7" />

                <div class="date"><span class="dayotw">Ma</span><span class="day">14 Nov</span></div>

                <span class="copyright-img">Foto door Martin Waalboer</span>

              </div>



              <div class="content">

                <p>
                Oud-studenten van de Universiteit Twente hebben een relatief kleine, houten windmolen ontwikkeld. Deze molen wekt genoeg energie op voor zo'n acht huishoudens. Hun bedrijfje, EAZ Wind in het Groningse Overschild, verovert met de uitvinding momenteel het Groningse platteland. Tientallen molens hebben ze al verkocht.
                </p>
                <p>
                Volgend jaar hopen ze er dertig te produceren. Maar hun droom reikt natuurlijk verder dan Groningen: ook elders in Nederland zouden de houten molens moeten verschijnen.
                </p>
                <p>            
                "We krijgen veel aanvragen, ook bijvoorbeeld uit Friesland. Maar de bestemmingsplannen daar staan kleine windmolens van 15 meter (nog) niet toe", zegt Timo Spijkerboer van EAZ ("staat voor Enschede aan Zee, is een grapje uit onze studententijd, omdat we liever aan zee woonden met veel wind").
                </p>
          
              </div>



              <div class="buy table-responsive">

                <h3>Koop kaarten</h3>

                <!-- form taken from current USVA website to give some direction -->
                <form method="post" action="index.php?id=200&amp;L=">
                  <table>
                    <tbody><tr>
                      <td><input class="seats" type="text" name="m[1][seats]" size="3" value="0">&times;</td>
                      <td class="amount">€&nbsp;3.00</td>
                      <td>Studenten</td>
                      <td>
                        <input type="hidden" name="m[1][price]" value="3.00">
                        <input type="hidden" name="m[1][title]" value="Flanorlezing met Tjitske Jansen - dinsdag 17 november 2015 (Studenten)">
                        <input type="hidden" name="m[1][puid]" value="1012_regular">
                      </td>
                    </tr>
                    <tr>
                      <td><input class="seats" type="text" name="m[2][seats]" size="3" value="0">&times;</td>
                      <td class="amount">€&nbsp;5.00</td>
                      <td>Niet-studenten/overig</td>
                      <td>
                        <input type="hidden" name="m[2][price]" value="5.00">
                        <input type="hidden" name="m[2][title]" value="Flanorlezing met Tjitske Jansen - dinsdag 17 november 2015 (Niet-studenten/overig)">
                        <input type="hidden" name="m[2][puid]" value="1012_special">
                      </td>
                    </tr>
                    <tr>
                      <td><input class="seats" type="text" name="m[3][seats]" size="3" value="0">&times;</td>
                      <td class="amount">€&nbsp;3.00</td>
                      <td>Vrijwilligers USVA</td>
                      <td>
                        <input type="hidden" name="m[3][price]" value="3.00">
                        <input type="hidden" name="m[3][title]" value="Flanorlezing met Tjitske Jansen - dinsdag 17 november 2015 (Vrijwilligers USVA)">
                        <input type="hidden" name="m[3][puid]" value="1012_extra">
                      </td>
                    </tr>
                  </tbody></table>
                  <input type="submit" class="btn btn-red btn-lg mt10" value="Toevoegen aan winkelmandje">
                  <input type="hidden" name="usva_order_referer" value="http://www.usva.nl/index.php?id=64&amp;tx_cal_controller[view]=event&amp;tx_cal_controller[type]=tx_cal_phpicalendar&amp;tx_cal_controller[uid]=1012&amp;tx_cal_controller[lastview]=view-list%7Cpage_id-64&amp;tx_cal_controller[year]=2015&amp;tx_cal_controller[month]=11&amp;tx_cal_controller[day]=17&amp;cHash=cd4cc82eb8936be1fd56853f9f61ebee">
                </form>

              </div>

              <div class="clearfix"></div>




            </article>

          </div>





          <div class="col-md-4 col-md-pull-8">


            <div class="usva-widget widget-link">

              <a class="more" href="agenda.php"><i class="fa fa-arrow-left mr10"></i>Terug naar het maandoverzicht</a>

            </div>


            <div class="usva-widget widget-agenda">
              <h2 class="title">Ook binnenkort</h2>

              <table class="table">
                
                <tr data-href="agenda-item.php">
                  <td class="date"><span class="day">14</span><span class="month">Nov</span></td>
                  <td class="item">Open Jazz Session</td>
                </tr>
                
                <tr data-href="agenda-item.php">
                  <td class="date"><span class="day">15</span><span class="month">Dec</span></td>
                  <td class="item">Mira in Concert</td>
                </tr>

                <tr data-href="agenda-item.php">
                  <td class="date"><span class="day">1</span><span class="month">Jan</span></td>
                  <td class="item">Eindoptreden Estrellas</td>
                </tr> 

              </table>

              <a class="more" href="agenda.php">Bekijk de hele agenda<i class="fa fa-arrow-right ml10"></i></a>

            </div>

          </div>


        </section>

      </div>









    <?php include('footer.php'); ?>
